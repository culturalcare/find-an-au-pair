export class Aupair {
	public _id: string;
	public index: number;
	public name: string;
	public picture: string;
	public about: string;
	public englishLevel: number;
	public country: string;
	public dateOfBirth: Date;
	public gender: 'F'|'M';
}
